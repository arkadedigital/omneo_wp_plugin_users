<div class="wrap" ng-app="users">
    <h1>
        App Users
    </h1>
    <br>

    <div ng-controller="UsersListCtrl" ng-cloak>
        <div class="notice notice-info" ng-show="is_loading">
            <p><span class="spinner is-active" style="float:left;margin-top:0;"></span> Loading users...</p>
        </div>


        <div ng-if="!is_loading && users.data.length > 0">
            <div style="float:right;" ng-if="max_pages > 1">Page {{ current_page }} of {{ max_pages }}</div>
            <h3>Total App Users: {{ users.meta.total_pages }}</h3>
        </div>

        <table class="wp-list-table widefat striped posts" ng-if="!is_loading && users.data.length > 0">
            <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Role</td>
                <td>Approved</td>
                <td></td>
            </tr>
            </thead>

            <tbody>
            <tr ng-repeat="user in users.data">
                <td>{{ user.id }}</td>
                <td>{{ user.email }}</td>
                <td>{{ user.role_details.title }}</td>
                <td>{{ user.approved }}</td>
                <td>
                    <div>
                        <a href="/wp-admin/admin.php?page=app-user-view&id={{ user.id }}" class="button"
                           ng-click="view_segment(segment)">
                            View
                        </a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <div class="notice notice-info" ng-if="!is_loading && segments.length === 0">
            <p>No users</p>
        </div>

        <br>

        <div class="pagination" ng-show="!is_loading && users.data.length > 0 && max_pages > 1">
            <div style="float:right;" ng-if="max_pages > 1">Page {{ current_page}} of {{ max_pages }}</div>
            <button type="button" class="button" ng-click="previous_page()" ng-disabled="current_page==1">&laquo; Previous</button>
            <button type="button" class="button" ng-class="{'button-primary': current_page==n}" ng-repeat="n in _.range(1,max_pages + 1)" ng-click="goto_page(n)" ng-disabled="current_page==n">{{ n }}</button>
            <button type="button" class="button" ng-click="next_page()" ng-disabled="current_page==max_pages">Next &raquo;</button>
        </div>
    </div>
</div>