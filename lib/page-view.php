<?php

$user_id = sanitize_key($_GET['id']);

// Get user roles from omneo api
$data['api_request'] = 'userroles';
$data['verb'] = 'get';
$userroles_response = \Omneo\Core\send_request($data);
$roles = [];
foreach($userroles_response['data'] as $role) {
    $roles[$role['id']] = $role['id'] . ': ' . $role['title'];
}

if (isset($_POST)) {
    $data['api_request'] = 'users/' . $_POST['id'];
    $data['data'] = array(
        'approved' => sanitize_text_field($_POST['approved']),
        'first_name' => sanitize_text_field($_POST['first_name']),
        'last_name' => sanitize_text_field($_POST['last_name']),
        'email' => sanitize_text_field($_POST['email']),
        'third_party_customer_id' => sanitize_text_field($_POST['third_party_customer_id']),
    );

    $data['verb'] = 'put';

    foreach ($_POST['attr'] as $a) {
        $data['data'][$a['key']] = $a['value'];
    }
    $data['data']['role_id'] = sanitize_text_field($_POST['role_id']);
    $response = \Omneo\Core\send_request($data);
}

$data['api_request'] = 'users/' . $user_id;
$data['data'] = array();
$data['verb'] = 'GET';

$response = \Omneo\Core\send_request($data);
?>
<br>

<div ng-app="users">
    <a class="button" href="/wp-admin/admin.php?page=app-users">< Back to User List</a>

    <form action="" method="post" ng-controller="EditUserCtrl">
        <h2>User Details</h2>
        <table class="wp-list-table widefat striped posts">
            <tbody>
            <tr>
                <td width="40%">Email</td>
                <td>
                    <input type="text" name="email" value="<?php echo $response['data']['email']; ?>" style="width: 100%">
                </td>
            </tr>
            <tr>
                <td>First Name</td>
                <td width="60%">
                    <input type="text" name="first_name" value="<?php echo $response['data']['first_name']; ?>" style="width: 100%">
                </td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <input type="text" name="last_name" value="<?php echo $response['data']['last_name']; ?>" style="width: 100%">
                </td>
            </tr>
            <tr>
                <td>Third Party Customer ID</td>
                <td><input type="text" name="third_party_customer_id" value="<?php echo $response['data']['third_party_customer_id'] ?>" style="width: 100%"></td>
            </tr>
            <tr>
                <td>User Role</td>
                <td>
                    <select name="role_id" style="width: 100%">
                        <?php foreach ($roles as $id => $role): ?>
                            <option value="<?php echo $id ?>"
                                <?php echo strtolower($response['data']['role_details']['id']) == $id ? ' selected ' : ''; ?>>
                                <?php echo ucwords($role) ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Access Key ID</td>
                <td><?php echo $response['data']['access_key_id'] ?></td>
            </tr>

            <tr>
                <td>Approved Status</td>
                <td>

                    <select name="approved" style="width: 100%">
                        <option value="1" <?php echo $response['data']['approved'] == 1 ? ' selected ' : ''; ?>>
                            Approved
                        </option>
                        <option value="0" <?php echo $response['data']['approved'] == 0 ? ' selected ' : ''; ?>>
                            Unapproved
                        </option>
                    </select>
                </td>
            </tr>

            </tbody>
        </table>


        <br>

        <h2>User Attributes</h2>

        <table class="wp-list-table widefat striped posts">
            <tbody>
            <?php if (!empty($response['data']['user_attributes'])) : ?>
                <?php foreach ($response['data']['user_attributes'] as $attr) : ?>
                    <?php if (!array_key_exists($attr['key'], $response['data']) && $attr['key'] != 'role_title'): ?>
                        <tr>
                            <td>
                                <input type="hidden" name="attr[B<?php echo $attr['id'] ?>][key]"
                                       value="<?php echo $attr['key'] ?>" style="width: 100%">

                                <?php echo $attr['key'] ?></td>
                            <td>
                                <input type="text" name="attr[B<?php echo $attr['id'] ?>][value]" value="<?php echo $attr['value'] ?>" style="width: 100%">
                            </td>
                            <td></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <tr ng-repeat="attribute in user_attributes">
                <td>
                    <input type="text" name="attr[{{ 'A' + $index }}][key]" ng-model="attribute.key" style="width:100%;" placeholder="Enter attribute name">
                </td>
                <td>
                    <input type="text" name="attr[{{ 'A' + $index }}][value]" value="" placeholder="Enter attribute value" style="width:100%;">
                </td>
                <td>
                    <button type="button" class="button" ng-click="remove_attribute(attribute)">Remove</button>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="100%">
                    <span ng-if="is_loading_attributes">Loading attributes... &nbsp;</span>

                    <select ng-show="!is_loading_attributes" ng-model="selected_attribute">
                        <option value="">-- New Attribute --</option>
                        <option ng-repeat="a in attributes" value="{{ a }}">{{ a }}</option>
                    </select>

                    <button class="button" type="button" ng-click="add_attribute(user_attributes.length)" ng-disabled="is_loading_attributes">
                        Add new attribute
                    </button>
                </td>
            </tr>
            </tfoot>
        </table>


        <?php if (!empty($response['data']['user_devices'])) : ?>
            <hr>
            <h2>User Devices</h2>

            <table class="wp-list-table widefat striped posts">
                <thead>
                <tr>
                    <td>Type</td>
                    <td>Vendor ID</td>
                    <td>Notification Token</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($response['data']['user_devices'] as $attr) : ?>
                    <tr>
                        <td width="30%"><?php echo $attr['device_type'] ?></td>
                        <td width="30%"><?php echo $attr['device_vendor_id'] ?></td>
                        <td width="40%"><?php echo $attr['device_push_notification_token'] ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>


        <br>
        <input type="hidden" name="id" value="<?php echo $user_id ?>">
        <input type="submit" value="Save User Details" class="button button-primary">

    </form>

    <?php //var_dump($response['data']['user_attributes'])?>
</div>
