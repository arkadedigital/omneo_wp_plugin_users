<?php namespace Omeno\Users;


use Omeno\Core;

function menu()
{
    add_menu_page(
        'App Users',
        'App Users',
        'manage_options',
        'app-users',
        __NAMESPACE__ . '\\page_app_users',
        'dashicons-groups'
    );

    add_submenu_page(
        null,
        'App Users',
        'App Users',
        'manage_options',
        'app-user-view',
        __NAMESPACE__ . '\\page_app_user_view',
        'dashicons-groups'
    );

}

add_action('admin_menu', __NAMESPACE__ . '\\menu');


function page_app_users()
{
    // Load page
    require_once(\Omneo\Users\__PATH() . '/lib/page-list.php');
}

function page_app_user_view()
{
    // Load page
    require_once(\Omneo\Users\__PATH() . '/lib/page-view.php');
}


function get_approval_pending_users()
{
    $data['api_request'] = 'users';
    $data['data'] = array(
        'approve' => 0
    );
    $data['verb'] = 'GET';

    $list = \Omneo\Core\send_request($data);

    return $list;
}

function load_assets()
{
    if (isset($_GET['page']) && in_array($_GET['page'], ['app-users', 'app-user-view'])) {
        wp_enqueue_script('jquery-ui', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'jquery-ui.min.js');
        wp_enqueue_script('lodash', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'lodash.min.js');
        wp_enqueue_style('toastr', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'toastr.min.css');
        wp_enqueue_script('toastr', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'toastr.min.js');
        wp_enqueue_script('angular', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular.min.js');
        wp_enqueue_script('angular-route', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular-route.min.js');
        wp_enqueue_script('angular-slugify', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular-slugify.js');
        wp_enqueue_script('angular-ui-sortable', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular-ui-sortable.min.js');
        wp_enqueue_script('users', plugins_url() . DS . 'omneo-users' . DS . 'assets' . DS . 'main.js');
    }
}

load_assets();


function users_send_request()
{
    $data = json_decode(stripslashes($_GET['data']), true);
    $response = \Omneo\Core\send_request($data);
    $json = json_encode($response);
    echo $json;
    exit;
}

add_action('wp_ajax_users_send_request', __NAMESPACE__ . '\\users_send_request');
