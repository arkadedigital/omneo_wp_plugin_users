<?php

use Core\Email;

function password_reset($data)
{

    if ($data['password_reset_token']) {
        $api = array();
        $api['api_request'] = 'userattributes';
        $api['verb'] = 'get';
        $api['data']['key'] = 'password_reset_token';
        $api['data']['value'] = $data['password_reset_token'];

        $user = \Omneo\Core\send_request($api);


        $user_link = site_url() . '/password-reset/?token=' . $user['data'][0]['value'];
        $user_email = $user['data'][0]['user']['email'];
        $user_name = $user['data'][0]['user']['first_name'] . ' ' . $user['data'][0]['user']['last_name'];

        $to = array('email' => $user_email, 'name' => $user_name);
        $from = array('email' => get_field('email_settings_from_email', 'option'), 'name' => get_field('email_settings_from_name', 'option') . ' - ' . get_bloginfo());
        $subject = 'Your password reset email';

        $message = "<p>Please click the link below to reset your password</p>";
        $message .= "<a href='{$user_link}'>{$user_link}</a>";

        $mail = Email\send_ses_email($to, $from, $subject, $message);
    }

}

function user_activation($data)
{

    if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL) || empty(($data['email']))) {
        return false;

    }


    $user_id = $data['id'];

    $data['api_request'] = 'users/' . $user_id;

    $data['verb'] = 'put';

    $data['data']['approval_token'] = sha1(md5('user_id:' . $user_id));

    log_email('User Activation Email REQUEST for ID ' . $user_id);

    $response = \Omneo\Core\send_request($data);


    if (!empty($response['data']['email'])) {

        $domain = substr(strrchr($response['data']['email'], "@"), 1);

        $a_email_domains = get_field('email_settings_white_list_domains', 'option');

        $approved_email_domains = explode(',', $a_email_domains);

        $approved_email_domains = array_map('trim', $approved_email_domains);

        if (in_array($domain, $approved_email_domains)) {

            log_email('User ID ' . $user_id . '(' . $response['data']['email'] . ') is white-listed');
            $mail = email_white_listed_user($response);

        } else {
            //send to admin
            log_email('User ID ' . $user_id . '(' . $response['data']['email'] . ') is NOT white-listed');
            $mail = email_site_admin($response);

        }

    } else {
        $mail = email_site_admin($response);
    }


    if ($mail == 1) {
        log_email('User Activation Email SUCCESS for ID ' . $user_id);
        header('HTTP/1.1 200 OK', true, 200);
    } else {
        log_email('User Activation Email FAILED for ID ' . $user_id);
        header('HTTP/1.1 500 Email Not Sent', true, 500);
    }


    log_email('Email Sent Status : ' . $mail);
    exit;
//  var_dump($_SERVER['HTTP_HOST']);

}


function email_site_admin($response)
{
    $user_id = $response['data']['id'];


    $user_link = site_url() . '/wp-admin/admin.php?page=app-user-view&id=' . $response['data']['id'];

    $to = array('email' => get_field('email_settings_account_approval_admin_email', 'option'), 'name' => get_field('email_settings_account_approval_admin_name', 'option'));
    $from = array('email' => get_field('email_settings_from_email', 'option'), 'name' => get_field('email_settings_from_name', 'option') . ' - ' . get_bloginfo());
    $subject = 'New user registration for ' . get_bloginfo() . ' (' . $response['data']['email'] . ')';

    if (empty($response['data']['email'])) {
        $subject = 'New user registration on ' . get_bloginfo();
    }

    $message = "A new user account have been registered on " . get_bloginfo();
    $message .= "<p>Please click the link below to activate the user account</p>";
    $message .= "<a href='{$user_link}'>{$user_link}</a>";

    $mail = Email\send_ses_email($to, $from, $subject, $message);

    return $mail;
}


function email_white_listed_user($response)
{
    //create a approval token
    $token = (get_user_attribute_value('approval_token', $response));

    if ($response['data']) {
        $user_link = site_url() . '/activate-account/?token=' . $token;
        $user_email = $response['data']['email'];
        $user_name = $response['data']['first_name'] . ' ' . $response['data']['last_name'];

        $to = array('email' => $user_email, 'name' => $user_name);
        $from = array('email' => get_field('email_settings_from_email', 'option'), 'name' => get_field('email_settings_from_name', 'option') . ' - ' . get_bloginfo());
        $subject = 'Your account activation email';

        $message = "<p>Please click the link below to activate your account</p>";
        $message .= "<a href='{$user_link}'>{$user_link}</a>";
    }

    $mail = Email\send_ses_email($to, $from, $subject, $message);

    return $mail;
}


function get_user_attribute_value($key, $data)
{
    $attribs = ($data['data']['user_attributes']);

    foreach ($attribs as $att) {
        if ($att['key'] == $key) {
            return $att['value'];
        }
    }
}

