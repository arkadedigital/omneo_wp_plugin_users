<?php namespace Omneo\Users;

use Omneo\Core;

defined('ABSPATH') or die('Access Denied');

/**
 * Plugin Name:     Omneo Users
 * Plugin URI:      http://www.omneo.com.au
 * Version:         1.0.15
 * Description:     Omneo Users plugin
 */

// Load acf json files
//Core\register_acf_from_json(__PATH());


function __PATH()
{
    return plugin_dir_path(__FILE__);
}

require_once(__PATH() . '/lib/functions.php');
require_once(__PATH() . '/lib/webhook.php');