var app = angular.module('users', ['ngRoute', 'ui.sortable', 'slugifier']);

/**
 * List controller
 */
app.controller('UsersListCtrl', function ($scope, usersService) {

    $scope.current_page = 1;
    $scope.limit = 200;
    $scope._ = _; // Set lodash in scope

    $scope.get_users = function() {
        $scope.is_loading = true;
        $scope.offset = $scope.limit * ($scope.current_page - 1);

        // Get segments
        var args = {
            limit: $scope.limit,
            offset: $scope.offset
        };

        usersService.getUsers(args).then(function (data) {
            $scope.is_loading = false;
            $scope.users = data;
            $scope.max_pages = Math.ceil($scope.users.meta.total_pages / $scope.limit)
        });
    };
    $scope.get_users();

    $scope.goto_page = function(page) {
        $scope.current_page = page;
        $scope.get_users();
    };

    $scope.previous_page = function() {
        $scope.current_page -= 1;
        $scope.get_users();
    };

    $scope.next_page = function() {
        $scope.current_page += 1;
        $scope.get_users();
    }

});

/**
 * Edit user controller
 */
app.controller('EditUserCtrl', function ($scope, userAttributesService) {
    $scope.is_loading_attributes = true;
    $scope.user_attributes = [];

    // Get attribute keys
    userAttributesService.getAttributes().then(function (data) {
        $scope.is_loading_attributes = false;
        $scope.attributes = _.without(data, 'role_id', 'role_title', 'first_name', 'last_name', 'third_party_customer_id', 'email', 'approved');
    });

    $scope.add_attribute = function(index) {
        if($scope.selected_attribute) {
            $scope.user_attributes.push({
                id: index,
                key: $scope.selected_attribute
            });
        } else {
            $scope.user_attributes.push({id: index});
        }
    };

    $scope.remove_attribute = function(obj) {
        $scope.user_attributes = _.remove($scope.user_attributes, function(i) {
            return obj.id !== i.id
        });
    };

});

/**
 * Users service
 */
app.factory('usersService', function ($http, $q) {
    return {
        /**
         * Get users
         */
        getUsers: function (args) {
            return $http({
                url: ajaxurl,
                method: 'GET',
                params: {
                    action: 'users_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'users',
                        data: args
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data;
            });
        }


    };
});

/**
 * User Attributes service
 */
app.factory('userAttributesService', function ($http) {
    return {
        getUserAttributes: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'users_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'userattributekeys'
                    }
                }
            }).then(function (response) {
                return response.data.data;
            });
        },

        getAttributes: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'users_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'userattributekeys'
                    }
                }
            }).then(function (response) {
                return response.data.data;
            });
        }
    };
});
